#!/usr/bin/env bash

set -e

IMG_FILE=nixos.iso
OVMF_DIR=../firmware

mac="c2:67:4f:53:29:cc"
host_net="enp5s0"
macvtap_name="macvtap0nixws"

if ! ip link show "${macvtap_name}" ; then
	ip link add link "${host_net}" name "${macvtap_name}" type macvtap
	ip link set "${macvtap_name}" address "${mac}" up
fi

tapindex=$(cat /sys/class/net/"${macvtap_name}"/ifindex)
tapdevice="/dev/tap${tapindex}"

cloud-hypervisor \
	1> nixws.ch.stdout.log \
	2> nixws.ch.stderr.log \
	--api-socket /var/run/cloud-hypervisor.socket \
	-v \
	--seccomp log \
	--log-file ./nixws.log \
	--cpus boot=32 \
	--memory size=32G \
	--kernel "${OVMF_DIR}/CLOUDHV.fd" \
	--serial tty \
	--console off \
	--disk path="${IMG_FILE}" \
	--device \
		path=/sys/bus/pci/devices/0000:08:00.0 \
		path=/sys/bus/pci/devices/0000:08:00.1 \
		path=/sys/bus/pci/devices/0000:0a:00.3 \
		path=/sys/bus/pci/devices/0000:07:00.1 \
		path=/sys/bus/pci/devices/0000:07:00.3 \
	--net fd=3,mac="${mac}" 3<>$"${tapdevice}" \

	#--platform num_pci_segments=3,iommu_segments=1 \
	#--kernel $(which hypervisor-fw) \
