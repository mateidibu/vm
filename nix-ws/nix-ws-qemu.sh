#!/bin/sh

set -e

# ISO_FILE='nixos-minimal-23.11.4080.fb0c047e30b6-x86_64-linux.iso'
IMG_FILE='nix-ws-disk.raw'
OVMF_DIR=../firmware

mac=c2:67:4f:53:29:cc
host_net="enp5s0"
macvtap_name="macvtap0nixws"

if ! ip link show "${macvtap_name}" ; then
	ip link add link "${host_net}" name "${macvtap_name}" type macvtap
	ip link set "${macvtap_name}" address "${mac}" up
fi

tapindex="$(cat /sys/class/net/"${macvtap_name}"/ifindex)"
tapdevice="/dev/tap${tapindex}"

qemu-system-x86_64 \
	-D nix-ws-qemu.log \
	-nodefaults \
	-drive if=pflash,format=raw,unit=0,file="${OVMF_DIR}/OVMF_CODE.fd",readonly=on \
	-drive if=pflash,format=raw,unit=1,file=./OVMF_VARS.fd \
	-machine q35,accel=kvm \
	-enable-kvm \
	-cpu host,topoext=on \
	-smp cpus=32,cores=16,threads=2 \
	-object memory-backend-file,id=hugepages,size=16G,mem-path=/dev/hugepages,prealloc=on \
	-machine memory-backend=hugepages \
	-m 16G \
	-monitor stdio \
	-device vfio-pci,host=0000:0a:00.0 \
	-device vfio-pci,host=0000:0a:00.1 \
        -device vfio-pci,host=0000:0c:00.3 \
        -device vfio-pci,host=0000:07:00.1 \
        -device vfio-pci,host=0000:07:00.3 \
	-drive format=raw,if=none,id=root,file="${IMG_FILE}" \
	-device virtio-blk-pci,drive=root,disable-legacy=on \
	-net nic,model=virtio,macaddr="${mac}" -net tap,fd=3 3<>"${tapdevice}" \
	-vga none \
	-nographic \

	#-drive file=./$ISO_FILE,index=0,media=cdrom \
	#-kernel $(which hypervisor-fw) \
