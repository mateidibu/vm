#!/usr/bin/env bash

set -ex

VM_STORAGE_DIR="/mnt/vm-storage/"

IMG_FILE="$VM_STORAGE_DIR/win11.qcow2"
# STEAM_IMG_FILE="$VM_STORAGE_DIR/win11_steam.qcow2"
# WIN_ISO_FILE="$VM_STORAGE_DIR/Win11_23H2_English_x64v2.iso"
# WIN_ISO_FILE="$VM_STORAGE_DIR/Win10_22H2_English_x64v1.iso"
# VIRTIO_ISO_FILE="$VM_STORAGE_DIR/virtio-win-0.1.248.iso"
OVMF_DIR="$VM_STORAGE_DIR/firmware"

mac="c2:67:4f:53:29:cb"
host_net="enp5s0"
macvtap_name="macvtap0windows"

if ! ip link show "${macvtap_name}" ; then
    ip link add link "${host_net}" name "${macvtap_name}" type macvtap
    ip link set "${macvtap_name}" address "${mac}" up
fi

tapindex=$(cat /sys/class/net/"${macvtap_name}"/ifindex)
tapdevice="/dev/tap${tapindex}"

qemu-system-x86_64 \
    -nodefaults \
    -machine q35,accel=kvm \
    -enable-kvm \
    -cpu host,topoext=on \
    -smp cpus=32,cores=16,threads=2 \
    -object memory-backend-file,id=hugepages,size=32G,mem-path=/dev/hugepages,prealloc=on \
    -machine memory-backend=hugepages \
    -m 32G \
    -drive if=pflash,format=raw,unit=0,file="${OVMF_DIR}/OVMF_CODE.fd",readonly=on \
    -drive if=pflash,format=raw,unit=1,file="${OVMF_DIR}/OVMF_VARS.fd" \
    -drive format=qcow2,if=none,id=root,file="${IMG_FILE}" \
    -device virtio-blk-pci,drive=root,disable-legacy=on \
    -device vfio-pci,host=0000:08:00.0 \
    -device vfio-pci,host=0000:08:00.1 \
    -device vfio-pci,host=0000:0a:00.3 \
    -device vfio-pci,host=0000:07:00.1 \
    -device vfio-pci,host=0000:07:00.3 \
    -drive file="${WIN_ISO_FILE}",media=cdrom \
    -net nic,model=virtio,macaddr="${mac}" -net tap,fd=3 3<>"${tapdevice}" \
    -vga none \
    -nographic \

    # -drive file="${IMG_FILE}",media=disk,if=virtio \
    # -drive file="${VIRTIO_ISO_FILE}",media=cdrom \
    # -drive file="${STEAM_IMG_FILE}",media=disk,if=virtio \
    #-drive format=raw,if=none,id=steam,file="${STEAM_IMG_FILE}" \
    #-device virtio-blk-pci,drive=steam,disable-legacy=on \
    # -cdrom "${WIN_ISO_FILE}" \
    # -drive file="${VIRTIO_ISO_FILE}",index=0,media=cdrom \
    # -object iommufd,id=iommufd0 \
    # -device vfio-pci,host=0000:08:00.0,iommufd=iommufd0 \
    # -object iommufd,id=iommufd1 \
    # -device vfio-pci,host=0000:08:00.1,iommufd=iommufd1 \
    # -object iommufd,id=iommufd2 \
    # -device vfio-pci,host=0000:0a:00.3,iommufd=iommufd2 \
    # -object iommufd,id=iommufd3 \
    # -device vfio-pci,host=0000:07:00.1,iommufd=iommufd3 \
    # -object iommufd,id=iommufd4 \
    # -device vfio-pci,host=0000:07:00.3,iommufd=iommufd4 \
    # -monitor stdio \
    # -vga std \
    # -vnc :0 \
