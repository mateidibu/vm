#! /usr/bin/env nix-shell
#! nix-shell -i bash -p cloud-hypervisor OVMF-cloud-hypervisor.fd virtio-win

set -ex

VM_STORAGE_DIR="/mnt/vm-storage/"

IMG_FILE="$VM_STORAGE_DIR/win11.img"
FIRMWARE="$(nix eval --raw nixpkgs#OVMF-cloud-hypervisor.fd.outPath)/FV/CLOUDHV.fd"

mac="c2:67:4f:53:29:cb"
host_net="enp5s0"
macvtap_name="macvtap0windows"

if ! ip link show "${macvtap_name}" ; then
    ip link add link "${host_net}" name "${macvtap_name}" type macvtap
    ip link set "${macvtap_name}" address "${mac}" up
fi

tapindex=$(cat /sys/class/net/"${macvtap_name}"/ifindex)
tapdevice="/dev/tap${tapindex}"

cloud-hypervisor \
    --api-socket /var/run/cloud-hypervisor.socket \
    --seccomp log \
    --log-file ./windows.log \
    --cpus boot=32,kvm_hyperv=on \
    --memory size=16G \
    --kernel "${FIRMWARE}" \
    --serial tty \
    --console off \
    --disk path="${IMG_FILE}" \
    --device \
        path=/sys/bus/pci/devices/0000:0a:00.0 \
        path=/sys/bus/pci/devices/0000:0a:00.1 \
        path=/sys/bus/pci/devices/0000:0c:00.3 \
        path=/sys/bus/pci/devices/0000:07:00.1 \
        path=/sys/bus/pci/devices/0000:07:00.3 \
    --net fd=3,mac="${mac}" 3<>$"${tapdevice}" \

    # --disk path="${IMG_FILE}" path="${WIN_ISO_FILE}",readonly=true path="${VIRTIO_ISO_FILE}",readonly=true \
    # 1> windows.ch.stdout.log \
    # 2> windows.ch.stderr.log \
        # path=/sys/bus/pci/devices/0000:08:00.1 \
    # --kernel "$(which hypervisor-fw)" \
